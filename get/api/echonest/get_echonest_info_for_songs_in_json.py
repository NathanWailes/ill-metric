# -*- coding: UTF-8 -*-
from collections import defaultdict

from pyechonest import song

from get_songsdb_artist_name_to_echonest_artist_name import get_songsdb_artist_name_to_echonest_artist_name_given
from utils import export_data_to_a_json_file, get_json_data_given_filename, query_echonest


path_to_json_with_songs_not_found_on_echonest = 'songs_not_found_on_echonest.json'


def main(input_songsdb_path, output_song_data_path):
    songsdb_artist_name_to_echonest_artist_name = get_songsdb_artist_name_to_echonest_artist_name_given(input_songsdb_path)
    songs_remaining_to_get_data_for = get_list_of_songs_remaining_to_get_data_for(input_songsdb_path, output_song_data_path, songsdb_artist_name_to_echonest_artist_name)
    print("Starting to get new data for songs.")
    for song_to_get_data_for in songs_remaining_to_get_data_for:
        songsdb_song_artist = song_to_get_data_for['Primary Artist']
        songsdb_song_title = song_to_get_data_for['Title']
        if artist_and_title_were_not_found_on_echonest(songsdb_song_artist, songsdb_song_title):
            continue
        try:
            song_artist = songsdb_artist_name_to_echonest_artist_name[songsdb_song_artist]
        except KeyError:  # TODO: Remove this try/except once you get the songsdb-->echonest name dict filled out.
            continue
        echonest_data_for_song = get_echonest_info_for_song(song_artist, songsdb_song_title)
        if echonest_data_for_song:
            try:
                print("Exporting Data for " + song_artist.encode('ascii', 'replace') + ": " + songsdb_song_title.encode('ascii', 'replace'))
            except:
                import pdb; pdb.set_trace()
            export_data_for_song(songsdb_song_artist, songsdb_song_title, echonest_data_for_song, output_song_data_path)
        else:  # TODO: Export something for data where you didn't find anything, so you don't keep querying EchoNest for it every time you restart.
            print("No data found for " + song_artist.encode('ascii', 'replace') + ": " + songsdb_song_title.encode('ascii', 'replace'))
            export_artist_and_title_to_a_file_tracking_unfound_songs(songsdb_song_artist, songsdb_song_title)


def get_list_of_songs_remaining_to_get_data_for(songsdb_path, output_song_data_path, songsdb_artist_name_to_echonest_artist_name):
    print("Figuring out which of the input songs I've already gotten data for and which I still need to do.")
    songs_json = get_json_data_given_filename(songsdb_path)
    songs_remaining_to_get_data_for = []
    finished_songs_keyed_by_songsdb_names = get_finished_songs_keyed_by_songsdb_names(output_song_data_path)

    for song_json in songs_json:
        songsdb_artist = song_json['Primary Artist']
        if songsdb_artist in finished_songs_keyed_by_songsdb_names.keys():
            if song_json['Title'] in finished_songs_keyed_by_songsdb_names[songsdb_artist]:
                continue
        songs_remaining_to_get_data_for.append(song_json)
    return songs_remaining_to_get_data_for


def get_finished_songs_keyed_by_songsdb_names(output_song_data_path):
    try:
        finished_data = get_json_data_given_filename(output_song_data_path)
    except IOError:
        finished_data = {}
    finished_songs_keyed_by_songsdb_names = defaultdict(set)
    for echonest_artist_name, songs_data in finished_data.items():
        for echonest_song_title, echonest_data in songs_data.items():
            songsdb_artist_name = echonest_data['songsdb_artist_name']
            songsdb_title = echonest_data['songsdb_title']
            finished_songs_keyed_by_songsdb_names[songsdb_artist_name].add(songsdb_title)
    return finished_songs_keyed_by_songsdb_names


def artist_and_title_were_not_found_on_echonest(songsdb_song_artist, songsdb_song_title):
    try:
        unfound_songs_json = get_json_data_given_filename(path_to_json_with_songs_not_found_on_echonest)
    except IOError:
        unfound_songs_json = {}
    if songsdb_song_artist in unfound_songs_json.keys():
        if songsdb_song_title in unfound_songs_json[songsdb_song_artist]:
            return True
    return False


def get_echonest_info_for_song(artist_name, song_name):
    song_id = get_echonest_song_id_for_song(artist_name, song_name)
    if song_id:
        song_info_search_results = query_echonest(song.profile, ids=[song_id], buckets=['audio_summary'])
        song_data = song_info_search_results[0]
        data_to_return = {'artist_name': song_data.artist_name,
                          'title': song_data.title,
                          'song_id': song_data.id,
                          'audio_summary': song_data.cache['audio_summary']}
        return data_to_return
    else:
        return None


def get_echonest_song_id_for_song(artist_name, song_name):
    song_results = query_echonest(song.search, title=song_name, artist=artist_name)
    if song_results:
        first_song_result = song_results[0]
        song_id = first_song_result.id
        return song_id
    else:
        return None


def export_data_for_song(songsdb_song_artist, songsdb_song_title, echonest_song_data, output_song_data_path):
    try:
        existing_data = get_json_data_given_filename(output_song_data_path)
    except (IOError, ValueError):
        existing_data = {}

    artist_name = echonest_song_data['artist_name']
    song_title = echonest_song_data['title']
    if artist_name in existing_data.keys():
        existing_data[artist_name][song_title] = echonest_song_data
    else:
        existing_data[artist_name] = {song_title: echonest_song_data}

    # I need to save this info so I can restart the process in the middle and know which songs I've already done.
    existing_data[artist_name][song_title]['songsdb_artist_name'] = songsdb_song_artist
    existing_data[artist_name][song_title]['songsdb_title'] = songsdb_song_title

    export_data_to_a_json_file(existing_data, output_song_data_path)


def export_artist_and_title_to_a_file_tracking_unfound_songs(songsdb_song_artist, songsdb_song_title):
    try:
        existing_data = get_json_data_given_filename(path_to_json_with_songs_not_found_on_echonest)
    except (IOError, ValueError):
        existing_data = {}
    if songsdb_song_artist in existing_data.keys():
        existing_data[songsdb_song_artist].append(songsdb_song_title)
    else:
        existing_data[songsdb_song_artist] = [songsdb_song_title]

    export_data_to_a_json_file(existing_data, path_to_json_with_songs_not_found_on_echonest)

if __name__ == '__main__':
    main('SongsDB.json', 'echonest_song_data.json')

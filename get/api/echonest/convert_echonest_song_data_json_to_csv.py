import csv

from utils import get_json_data_given_filename


def main(input_json_path, output_csv_path):
    echonest_song_data = get_json_data_given_filename(input_json_path)
    export_echonest_song_data_to_csv(echonest_song_data, output_csv_path)
    return


def export_echonest_song_data_to_csv(echonest_song_data, output_csv_path):
    with open(output_csv_path, 'wb') as outfile:
        fieldnames = ['EchoNest Primary Artist', 'SongsDB Primary Artist', 'EchoNest Title', 'SongsDB Title',
                      'EchoNest Song ID', 'Time Signature', 'Energy', 'Liveness', 'Tempo', 'Speechiness', 'Acousticness',
                      'Instrumentalness', 'Mode', 'Key', 'Duration', 'Loudness', 'Valence', 'Danceability']
        writer = csv.DictWriter(outfile, fieldnames)
        writer.writeheader()
        for artist, songs_data in echonest_song_data.items():
            for song_title, song_data in songs_data.items():
                row = {}
                row['EchoNest Primary Artist'] = artist.encode('utf8')
                row['SongsDB Primary Artist'] = song_data['songsdb_artist_name'].encode('utf8')
                row['EchoNest Title'] = song_title.encode('utf8')
                row['SongsDB Title'] = song_data['songsdb_title'].encode('utf8')
                row['EchoNest Song ID'] = song_data['song_id']
                row['Time Signature'] = song_data['audio_summary']['time_signature']
                row['Energy'] = song_data['audio_summary']['energy']
                row['Liveness'] = song_data['audio_summary']['liveness']
                row['Tempo'] = song_data['audio_summary']['tempo']
                row['Speechiness'] = song_data['audio_summary']['speechiness']
                row['Acousticness'] = song_data['audio_summary']['acousticness']
                row['Instrumentalness'] = song_data['audio_summary']['instrumentalness']
                row['Mode'] = song_data['audio_summary']['mode']
                row['Key'] = song_data['audio_summary']['key']
                row['Duration'] = song_data['audio_summary']['duration']
                row['Loudness'] = song_data['audio_summary']['loudness']
                row['Valence'] = song_data['audio_summary']['valence']
                row['Danceability'] = song_data['audio_summary']['danceability']
                writer.writerow(row)
    return


if __name__ == '__main__':
    main('echonest_song_data.json', 'echonest_song_data.csv')

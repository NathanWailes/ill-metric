import json
import os
from socket import timeout
import time

from pyechonest import config
from pyechonest.util import EchoNestAPIError


class Memoize:
    def __init__(self, f):
        self.f = f
        self.memo = {}

    def __call__(self, *args):
        if args not in self.memo:
            self.memo[args] = self.f(*args)
        return self.memo[args]


def ensure_path_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)


def export_data_to_a_json_file(json_data, filename):
    with open(filename, 'w') as outfile:
        json.dump(json_data, outfile)


def get_json_data_given_filename(filename):
    with open(filename, 'r') as input_file:
        json_data = json.load(input_file)
    return json_data


@Memoize
def get_api_key_for(api_provider_name):
    api_keys = get_json_data_given_filename('api_keys.json')
    return api_keys[api_provider_name]


def query_echonest(query_function, *args, **kwargs):
    config.ECHO_NEST_API_KEY = get_api_key_for('echonest')
    while True:
        try:
            query_result = query_function(*args, **kwargs)
            time.sleep(3)  # A crude way of limiting myself to 120 requests per minute.
            return query_result
        except EchoNestAPIError, e:
            if 'The Identifier specified does not exist' in str(e):
                if '\\' in args:
                    import pdb; pdb.set_trace()
                else:
                    raise
            elif 'You are limited to 20 accesses every minute.' in str(e):
                print("API limit...waiting a few seconds.")
                time.sleep(5)
            elif 'Unknown error' in str(e):
                time.sleep(10)  # TODO: Figure out what to do with this type of error.
            else:
                raise
        except timeout:
            time.sleep(5)

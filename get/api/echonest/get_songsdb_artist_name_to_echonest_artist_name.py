# -*- coding: UTF-8 -*-
import json

from pyechonest import artist

from utils import export_data_to_a_json_file, get_json_data_given_filename, query_echonest


def get_songsdb_artist_name_to_echonest_artist_name_given(songsdb_path):
    songs_json = get_json_data_given_filename(songsdb_path)
    songsdb_artist_name_to_echonest_artist_name = get_json_data_artist_name_to_echonest_artist_name_given_filename('songsdb_artist_to_echonest_artist_name.json')
    '''for song_json in songs_json:  # I'm turning this off for now so I can test the rest of my code.
        songdb_artist_name = song_json['Primary Artist']
        if songdb_artist_name in songsdb_artist_name_to_echonest_artist_name:
            continue
        else:
            songsdb_artist_name_to_echonest_artist_name = get_updated_dict_given_new_artist_name(songsdb_artist_name_to_echonest_artist_name, songdb_artist_name)'''
    return songsdb_artist_name_to_echonest_artist_name


def get_updated_dict_given_new_artist_name(json_data_artist_name_to_echonest_artist_name, songdb_artist_name):
    echonest_artist_name = get_echonest_artist_name_given(songdb_artist_name)
    if echonest_artist_name:
        json_data_artist_name_to_echonest_artist_name[songdb_artist_name] = echonest_artist_name

        print(songdb_artist_name + ' --> ' + echonest_artist_name.encode('ascii', 'replace'))

        export_data_to_a_json_file(json_data_artist_name_to_echonest_artist_name, 'songsdb_artist_to_echonest_artist_name.json')
    return json_data_artist_name_to_echonest_artist_name


def get_json_data_artist_name_to_echonest_artist_name_given_filename(filename):
    try:
        with open(filename, 'r') as infile:
            return json.load(infile)
    except ValueError:
        return {}


def get_echonest_artist_name_given(songdb_artist_name):
    echonest_artist = query_echonest(artist.Artist, songdb_artist_name)
    if echonest_artist:
        return echonest_artist.name
    else:
        return None

import csv
from os.path import isfile
from random import randint
from time import sleep
from urllib.parse import urlencode
from get.scrape.utils import get_config, load_browser

pause = 2
path_to_artist_data = '../../../../data/artist_data.csv'


def main():
    """
    :return:
    """
    cfg = get_config()
    browser = load_browser(cfg)
    # artist_names_to_song_list_links = load_artist_names_to_song_list_links_from_csv()
    # export_artist_names_to_song_list_links(artist_names_to_song_list_links)
    # navigate_to_azlyrics_search_results_for_query('ASAP Rocky', browser)
    # print(azlyrics_search_returned_artist_results(browser))
    # import pdb; pdb.set_trace()
    # print(get_the_url_for_the_top_artist_result_when_searching_for_an_artist(browser))
    get_artist_song_list_links_for_rappers_and_rap_groups(browser)


def get_artist_song_list_links_for_rappers_and_rap_groups(browser):
    """
    :param browser:
    :return:
    """
    get_links_to_artists_song_lists_for_rappers(browser)
    get_links_to_artists_song_lists_for_rap_groups(browser)


def get_links_to_artists_song_lists_for_rappers(browser):
    """
    :param browser:
    :return:
    """
    file_path = "../../../../data/list of rappers.txt"
    get_artist_names_to_azlyrics_song_list_links_for_artists_in_file(file_path, browser)
    return


def get_links_to_artists_song_lists_for_rap_groups(browser):
    """
    :param browser:
    :return:
    """
    file_path = "../../../../data/list of rap groups.txt"
    get_artist_names_to_azlyrics_song_list_links_for_artists_in_file(file_path, browser)
    return


def get_artist_names_to_azlyrics_song_list_links_for_artists_in_file(path_to_file_with_artist_names, browser):
    """
    :param path_to_file_with_artist_names:
    :param browser:
    :return: dict
    """
    with open(path_to_file_with_artist_names, "r", encoding='utf-8') as infile:
        list_of_artist_names = infile.read().splitlines()
    artist_names_to_song_list_links = get_artist_names_to_azlyrics_song_list_links_for_artists(list_of_artist_names, browser)
    return artist_names_to_song_list_links


def get_artist_names_to_azlyrics_song_list_links_for_artists(list_of_artist_names, browser):
    """
    :param list_of_artist_names: list
    :param browser:
    :return: dict
    """
    artist_names_to_song_list_links = load_artist_names_to_song_list_links_from_csv()
    for artist_name in list_of_artist_names:
        if artist_name in artist_names_to_song_list_links.keys():
            pass
        else:
            navigate_to_azlyrics_search_results_for_query(artist_name, browser)
            artist_names_to_song_list_links[artist_name] = get_the_url_for_the_top_artist_result_when_searching_for_an_artist(browser)
            # it's probably bad practice to export after each artist...
            export_artist_names_to_song_list_links(artist_names_to_song_list_links)
        sleep(randint(1, 5))
    return artist_names_to_song_list_links


def load_artist_names_to_song_list_links_from_csv():
    artist_names_to_song_list = {}
    if isfile(path_to_artist_data):
        with open(path_to_artist_data) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                artist_names_to_song_list[row['artist_name']] = row['azlyrics_song_list_link']
    else:
        artist_names_to_song_list = {}
    return artist_names_to_song_list


def navigate_to_azlyrics_search_results_for_query(query, browser):
    url_to_search_azlyrics = get_azlyrics_search_url_for_query(query)
    browser.get(url_to_search_azlyrics)


def get_azlyrics_search_url_for_query(query):
    """
    :param query: str
    :return: str
    """
    params = {'q': query}  # ex: {'q': b'Kanye West'}
    encoded_params = urlencode(params)  # ex: "q=Kanye+West"
    url = 'http://search.azlyrics.com/search.php?%s' % encoded_params
    return url


def get_the_url_for_the_top_artist_result_when_searching_for_an_artist(browser):
    """
    :param browser:
    """
    # find the divs with class 'panel'
    # for each div, check if it has 'Artist results'
    # if it does, look for the element that has the link for the top artist
    panels = browser.find_elements_by_class_name('panel')
    for panel in panels:
        if 'Artist results:' in panel.text:
            element = browser.find_element_by_tag_name('a')
            url_for_the_top_artist_result = element.get_attribute('href')
            return url_for_the_top_artist_result
    return ''


def export_artist_names_to_song_list_links(artist_names_to_song_list_links):
    with open(path_to_artist_data, 'w', newline='') as csvfile:  # , newline=''
        fieldnames = ['artist_name', 'azlyrics_song_list_link']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for artist_name, azlyrics_song_list_link in artist_names_to_song_list_links.items():
            writer.writerow({'artist_name': artist_name, 'azlyrics_song_list_link': azlyrics_song_list_link})  # .encode('utf-8')


if __name__ == '__main__':
    main()
